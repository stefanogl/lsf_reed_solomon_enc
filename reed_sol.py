import sympy
sympy.var('al')
sympy.var('X')
E = 4
mod = 256

def add_mod2(a, b):
    t = []
    for i in range(len(a)):
        t.append(a[i]^b[i])
    return t

def left_shift(list):
    return list[1:]+[0]

base_family = [[0,0,0,0,0,0,0,1]]
family = [x for x in base_family]

p = [1,1,0,0,0,1,1,1]

for i in range(len(base_family), 2**len(p)):
    temp = left_shift(family[-1])
    if temp[0]:
        temp = add_mod2(temp, p)
    family.append(temp)

print(len(family))

G = 1
for j in range(128-E, 127+E):
    G *= (X - al**(11*j))

print(G)
print('developping')
G1 = sympy.expand(G)
print(G1)
print('complete')
G2 = sympy.collect(G1, X)

#dirty processing
G3 = str(G2)#.split(' ')
G3 = ''.join(['+' if x=='-' else x for x in G3])
G3_rev = (G3[-1::-1].split(')')[0])[-1::-1].strip(' ').strip('+')
print(G3_rev)
G3 = G3.split('(')
G3[0] = '1'
G3[1:] = [x.split(')')[0] for x in G3[1:]]
G3 = [x.split('+') for x in G3]
G3 = [[x for x in y if not '2*' in x] for y in G3]
G3 = ['0' if x==[] else x for x in G3]
G3 = G3 + [[G3_rev]]
G3 = [[x.strip(' ').split('**') for x in y if x != ''] for y in G3]
G3 = [ [x[1] if len(x) > 1 else x[0] for x in y]  for y in G3]
G3 = [ [int(x)%mod for x in y] for y in G3]
print(G3)
