/* -*- c++ -*- */
/*
 * Copyright 2021 Baptiste Fournier.
 * Based on GNU GPLv3 gr-ccsds library from André Løfaldli.
 * Based on GNU LGPL code from Phil Karn, KA9Q.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "rs_encode.h"
#include "ccsds.h"
#include "fixed.h"
#include "cstring"
#undef A0
#define A0 (NN) /* Special reserved value encoding zero in index form */

extern unsigned char ALPHA_TO[];
extern unsigned char INDEX_OF[];
extern unsigned char GENPOLY[];
extern unsigned char DATADUAL[];
extern unsigned char PARITYDUAL[];

static void encode_rs_8_c(data_t *data, data_t *parity,int pad){
  {
    int i, j;
	data_t feedback;

    std::memset(parity, 0, NROOTS*sizeof(data_t));

    encodeMainLoop: for(i=0;i<NN-NROOTS-PAD;i++){
		feedback = INDEX_OF[data[i] ^ parity[0]];
		if(feedback != A0){
			encodeFeedbackLoop: for(j=1;j<NROOTS;j++){
				parity[j] ^= ALPHA_TO[MODNN(feedback + GENPOLY[NROOTS-j])]; // weird modulo but better synthesis
			} 
		}
      /* Shift */
      std::memmove(&parity[0],&parity[1],sizeof(data_t)*(NROOTS-1));
      if(feedback != A0)
        parity[NROOTS-1] = ALPHA_TO[MODNN(feedback + GENPOLY[0])];
      else
        parity[NROOTS-1] = 0;
    }
  }

}

void encode_rs_ccsds(data_t *data, data_t *parity,int pad){
  int i;
  data_t cdata[NN-NROOTS];

  /* Convert data from dual basis to conventional */
  convertDataDual: for(i=0;i<NN-NROOTS-pad;i++)
    cdata[i] = DATADUAL[data[i]];

  encode_rs_8_c(cdata,parity,pad);

  /* Convert parity from conventional to dual basis */
  convertParityDual: for(i=0;i<NROOTS;i++)
    parity[i] = PARITYDUAL[parity[i]];
}

void rs_encode(uint8_t *data, bool use_dual_basis){
#pragma HLS TOP name=rs_encode
#pragma HLS INTERFACE s_axilite port=use_dual_basis
	// TODO differentiate input and output data structure to allow FIFO ?
	if (use_dual_basis) {
		encode_rs_ccsds(data, &data[RS_DATA_LEN],0);
	} else {
		encode_rs_8_c(data, &data[RS_DATA_LEN], 0);
	}
}
